﻿using System;
using System.Collections.Generic;

namespace api
{
    public partial class Users
    {
        public Guid Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Location { get; set; }
        public float Lat { get; set; }
        public float Long { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
