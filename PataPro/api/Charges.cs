﻿using System;
using System.Collections.Generic;

namespace api
{
    public partial class Charges
    {
        public Guid Id { get; set; }
        public Guid ProId { get; set; }
        public float Amount { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
