﻿using System;
using System.Collections.Generic;

namespace api
{
    public partial class Feedbacks
    {
        public Guid Id { get; set; }
        public Guid ProId { get; set; }
        public Guid UserId { get; set; }
        public string Comment { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
