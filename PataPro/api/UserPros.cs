﻿using System;
using System.Collections.Generic;

namespace api
{
    public partial class UserPros
    {
        public Guid Id { get; set; }
        public Guid ProId { get; set; }
        public Guid UserId { get; set; }
        public Guid ChargeId { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
