﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.viewModel
{
    public class proView
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Location { get; set; }
        public float Lat { get; set; }
        public float Long { get; set; }
        public string Occupation { get; set; }
        public string Description { get; set; }
    }
}
