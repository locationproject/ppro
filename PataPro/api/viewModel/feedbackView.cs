﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.viewModel
{
    public class feedbackView
    {
        public Guid ProId { get; set; }
        public Guid UserId { get; set; }
        public string Comment { get; set; }
    }
}
