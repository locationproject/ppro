﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.viewModel
{
    public class userProView
    {
        public Guid ProId { get; set; }
        public Guid UserId { get; set; }
        public Guid ChargeId { get; set; }
    }
}
