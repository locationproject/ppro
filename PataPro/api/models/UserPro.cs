﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.models
{
    public class UserPro
    {
        public Guid Id { get; set; }
        public Guid ProId { get; set; }
        public Guid UserId { get; set; }
        public Guid ChargeId { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
