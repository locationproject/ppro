﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.models
{
    public class Feedback
    {
        public Guid Id { get; set; }
        public Guid ProId { get; set; }
        public Guid UserId { get; set; }
        public string Comment { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
