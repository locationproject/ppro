﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.models
{
    public class Charge
    {
        public Guid Id { get; set; }
        public Guid ProId { get; set; }
        public float Amount { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
