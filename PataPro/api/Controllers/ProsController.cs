﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using api;
using api.models;
using api.viewModel;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProsController : ControllerBase
    {
        private readonly ApplicationDBContext _context;

        public ProsController(ApplicationDBContext context)
        {
            _context = context;
        }

        // GET: api/Pros
        [HttpGet]
        public IEnumerable<Pro> GetPro()
        {
            return _context.Pros;
        }

        [HttpPost("Login")]
        public Pro Login(Login login)
        {
            var pro = _context.Pros.Where(x => x.Email == login.Email).FirstOrDefault();
            if (pro != null)
            {
                if (pro.Password == login.Password)
                {
                    return pro;
                }
            }
            return null;
        }

        // GET: api/Pros/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPro([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pro = await _context.Pros.FindAsync(id);

            if (pro == null)
            {
                return NotFound();
            }

            return Ok(pro);
        }

        // PUT: api/Pros/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPro([FromRoute] Guid id, [FromBody] Pro pro)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pro.Id)
            {
                return BadRequest();
            }

            _context.Entry(pro).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Pros
        [HttpPost]
        public async Task<IActionResult> PostPro([FromBody] proView proview)
        {
            var pro = new Pro
            {
                Id = Guid.NewGuid(),
                Description = proview.Description,
                Email = proview.Email,
                Firstname = proview.Firstname,
                Lastname = proview.Lastname,
                Lat = proview.Lat,
                Location = proview.Location,
                Long = proview.Long,
                Occupation = proview.Occupation,
                Password = proview.Password,
                Phone = proview.Phone,
                Timestamp = DateTime.Now
            };

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Pros.Add(pro);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPro", new { id = pro.Id }, pro);
        }

        // DELETE: api/Pros/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePro([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pro = await _context.Pros.FindAsync(id);
            if (pro == null)
            {
                return NotFound();
            }

            _context.Pros.Remove(pro);
            await _context.SaveChangesAsync();

            return Ok(pro);
        }

        private bool ProExists(Guid id)
        {
            return _context.Pros.Any(e => e.Id == id);
        }
    }
}