﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using api;
using api.models;
using api.viewModel;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChargesController : ControllerBase
    {
        private readonly ApplicationDBContext _context;

        public ChargesController(ApplicationDBContext context)
        {
            _context = context;
        }

        // GET: api/Charges
        [HttpGet]
        public IEnumerable<Charge> GetCharge()
        {
            return _context.Charges;
        }

        // GET: api/Charges/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCharge([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var charge = await _context.Charges.FindAsync(id);

            if (charge == null)
            {
                return NotFound();
            }

            return Ok(charge);
        }

        // PUT: api/Charges/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharge([FromRoute] Guid id, [FromBody] Charge charge)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != charge.Id)
            {
                return BadRequest();
            }

            _context.Entry(charge).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ChargeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Charges
        [HttpPost]
        public async Task<IActionResult> PostCharge([FromBody] chargeView chargeview)
        {
            var charge = new Charge
            {
                Id = Guid.NewGuid(),
                Amount = chargeview.Amount,
                ProId = chargeview.ProId,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Charges.Add(charge);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharge", new { id = charge.Id }, charge);
        }

        // DELETE: api/Charges/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharge([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var charge = await _context.Charges.FindAsync(id);
            if (charge == null)
            {
                return NotFound();
            }

            _context.Charges.Remove(charge);
            await _context.SaveChangesAsync();

            return Ok(charge);
        }

        private bool ChargeExists(Guid id)
        {
            return _context.Charges.Any(e => e.Id == id);
        }
    }
}