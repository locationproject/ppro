﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using api;
using api.models;
using api.viewModel;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserProesController : ControllerBase
    {
        private readonly ApplicationDBContext _context;

        public UserProesController(ApplicationDBContext context)
        {
            _context = context;
        }

        // GET: api/UserProes
        [HttpGet]
        public IEnumerable<UserPro> GetUserPro()
        {
            return _context.UserPros;
        }

        // GET: api/UserProes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserPro([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userPro = await _context.UserPros.FindAsync(id);

            if (userPro == null)
            {
                return NotFound();
            }

            return Ok(userPro);
        }

        // PUT: api/UserProes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUserPro([FromRoute] Guid id, [FromBody] UserPro userPro)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userPro.Id)
            {
                return BadRequest();
            }

            _context.Entry(userPro).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserProExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UserProes
        [HttpPost]
        public async Task<IActionResult> PostUserPro([FromBody] userProView userProview)
        {
            var userPro = new UserPro
            {
                Id = Guid.NewGuid(),
                ChargeId = userProview.ChargeId,
                ProId = userProview.ProId,
                UserId = userProview.UserId,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.UserPros.Add(userPro);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUserPro", new { id = userPro.Id }, userPro);
        }

        // DELETE: api/UserProes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserPro([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userPro = await _context.UserPros.FindAsync(id);
            if (userPro == null)
            {
                return NotFound();
            }

            _context.UserPros.Remove(userPro);
            await _context.SaveChangesAsync();

            return Ok(userPro);
        }

        private bool UserProExists(Guid id)
        {
            return _context.UserPros.Any(e => e.Id == id);
        }
    }
}